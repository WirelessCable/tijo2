package com.company;

import java.util.Arrays;

class Main {

    public static void main(String[] args) {
        CsvGenerator csvGenerator = new CsvWriter();
        // add lines
        csvGenerator.ToCsv(Arrays.asList("first line", "second line", "third line"));
    }
}