package com.company;

import java.io.FileWriter;
import java.util.List;

class CsvWriter implements CsvGenerator {
    @Override
    public void ToCsv(List<String> list){
        try{
            FileWriter CsvW = new FileWriter("new.csv");
            // pretty line
            CsvW.append("*************");
            CsvW.append("\n");
            // new line + '\n'
            for(int i = 0; i < list.size(); i++){
                CsvW.append(list.get(i));
            }
            CsvW.append("\n");
            // pretty line
            CsvW.append("*************");
            CsvW.append("\n");
            CsvW.flush();
            CsvW.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}